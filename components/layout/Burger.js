import style from './burger.module.css'

export default function Burger() {
	return (
		<div className={`${style.burger} collapsed`}
			data-bs-toggle="collapse" href="#collapseExample"
			role="button" aria-expanded="false"
			aria-controls="collapseExample">
			<span></span>
			<span></span>
			<span></span>
		</div>
	)
}
