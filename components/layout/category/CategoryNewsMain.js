import style from './categoryNewsMain.module.css'
import Link from 'next/link'
import Image from 'next/image'

export default function CategoryNewsMain({ data }) {
	const { id, title, short_content, image, publication_date, url, category_url } = data
	let day, month, year

	const date = new Date(publication_date * 1000)
	day = date.getDay().toString().padStart(2, '0')
	month = (date.getMonth() + 1).toString().padStart(2, '0')
	year = date.getFullYear().toString().padStart(2, '0')

	const formattedDate = `${day}/${month}/${year}`

	return (
		<Link href={"/[category]/[article]"} as={`/${category_url}/${url}`}>
			<a className={style._body}>
				<div className="row">
					<div className={`${style._photo} col-lg-8`}>
						<Image src={image} width={550} height={330} />
					</div>

					<div className={`${style._content} col-lg-4`}>
						<div className={style._date}>
							<Image src="/icons/clock.svg" width={10} height={10} />
							{formattedDate}
						</div>
						<div>
							<h3 className={style._title}>
								{title}
							</h3>
							<p className={style._description}>
								{short_content}
							</p>
						</div>
					</div>
				</div>
			</a>
		</Link>
	)
}