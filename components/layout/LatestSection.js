import CategoryNewsItem from '/components/layout/category/CategoryNewsItem'
import style from './latestSection.module.css'
import { useTranslation } from 'next-i18next'

export default function LatestSection({ news }) {
	const { t } = useTranslation('common')

	return (
		<div className={style._body}>
			<div className="row">
				{news.map(item => (
					<div className="col-md-6 col-lg-3 py-lg-0" key={item.id}
						style={{ paddingTop: "15px", paddingBottom: "15px" }}>
						<CategoryNewsItem data={item} />
					</div>
				))}
			</div>
			<div className={style._showMore}>
				<button>
					{t("moreNewsLabel")}
					<img src="/icons/ellipse-arrow.svg" alt="more" />
				</button>
			</div>
		</div>

	)
}