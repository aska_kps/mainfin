import Image from 'next/image'
import Burger from './Burger.js'
import style from './header.module.css'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useTranslation } from 'next-i18next'

export default function Header({ data }) {
	const { t } = useTranslation('common')
	const router = useRouter()
	const { categories } = data

	return (
		<header className={style.header}>
			<div className="container">
				<div className={style.header__top}>
					<div className="row justify-content-between">
						<div className="col-8 col-md-3 mr-auto">
							<Link href="/">
								<a><img src="/icons/logo.svg" alt="Mainfin" title="Mainfin" /></a>
							</Link>
						</div>

						<div className="col-9 col-md-5 order-md-2 order-4 mt-4 mt-md-0">
							<div className={style.header__search}>
								<input type="text" name="search" placeholder={t("searchLabel")} />
								<button type="button">
									<img src="/icons/search.svg" alt={t("searchLabel")} />
								</button>
							</div>
						</div>

						<div className="col-4 col-md-2 order-2 order-md-3">
							<div className={style.header__languages}>
								{router.locales.map((locale) => (
									<Link href='/' locale={locale} key={locale}>
										<a>{locale}</a>
									</Link>
								))}
							</div>
						</div>
						<div className="col-3 d-md-none order-3 mt-4 d-flex align-items-center">
							<Burger />
						</div>
					</div>
				</div>

				<nav className={`${style.header__navigation} d-none d-md-block`}>
					<ul className="row justify-content-between text-center">
						<li className="w-auto">
							<Link href="/">
								<a className="current">{t("homeLabel")}</a>
							</Link>
						</li>

						{categories.map((category) => (
							<li className="w-auto" key={category.id}>
								<Link href={`/${category.url}`}>
									<a className="current">{category.title}</a>
								</Link>
							</li>
						))}
					</ul>
				</nav>

				<nav className={`${style.header__navigation} collapse`} id="collapseExample">
					<ul className="row flex-column justify-content-between text-center">
						<li className="w-auto">
							<Link href="/">
								<a className="current">{t("homeLabel")}</a>
							</Link>
						</li>

						{categories.map((category) => (
							<li className="w-auto" key={category.id}>
								<Link href={`/${category.url}`}>
									<a className="current">{category.title}</a>
								</Link>
							</li>
						))}
					</ul>
				</nav>
			</div>
		</header >
	)
}