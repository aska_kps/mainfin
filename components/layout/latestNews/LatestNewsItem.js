import Link from 'next/link'
import Image from 'next/image'
import style from './latestNewsItem.module.css'

export default function LatestNewsItem({ data }) {
	const { id, title, views, publication_date, image, url, category_url } = data
	let day, month, year

	const date = new Date(publication_date * 1000)
	day = date.getDay().toString().padStart(2, '0')
	month = (date.getMonth() + 1).toString().padStart(2, '0')
	year = date.getFullYear().toString().padStart(2, '0')

	const formattedDate = `${day}/${month}/${year}`

	return (
		<Link href={"/[category]/[article]"} as={`/${category_url}/${url}`}>
			<a className={style._body}>
				<div className={style._photo}>
					<Image src={image} width={60} height={60} alt={title} />
				</div>

				<div className={style._information}>
					<div className={style._title}>{title}</div>
					<div className={style._statistics}>
						<p>{formattedDate}</p>
						<p>{views}</p>
					</div>
				</div>
			</a>
		</Link>
	)
}