import LatestNewsItem from './LatestNewsItem'
import style from './latestNewsBlock.module.css'
import { useTranslation } from 'next-i18next'

export default function LatestNewsBlock({ news }) {
	const { t } = useTranslation('common')

	return (
		<div className={style._body}>
			<div className={style._header}>
				<p>{t("latestLabel")}</p>
			</div>
			<div className="row">
				{news.map((item) => (
					<div className="col-md-6 col-lg-12" key={item.id}>
						<LatestNewsItem data={item} />
					</div>
				))}
			</div>
		</div >
	)
}

