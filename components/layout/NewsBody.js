import Link from 'next/link'
import { useRouter } from 'next/router'
import styles from './newsBody.module.css'

export default function newsBoby({ data }) {
	const { title, publication_date, image, content } = data
	let day, month, year

	const date = new Date(publication_date * 1000)
	day = date.getDay().toString().padStart(2, '0')
	month = (date.getMonth() + 1).toString().padStart(2, '0')
	year = date.getFullYear().toString().padStart(2, '0')

	const formattedDate = `${day}/${month}/${year}`

	return (
		<div className={styles._body}>
			<h1 className={styles._title}>
				{title}
			</h1>

			<div className={styles._date}>
				<img src="/icons/clock-grey.svg" alt="Дата публикации" title="Дата публикации" />
				{formattedDate}
			</div>

			<div className={styles._poster}>
				<img src={image} alt={title} title={title} />
			</div>

			<div className={styles._content} dangerouslySetInnerHTML={{ __html: content }}></div>
		</div>
	)
}
