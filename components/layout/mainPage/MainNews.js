import Link from 'next/link'
import Image from 'next/image'
import style from './mainNews.module.css'

export default function MainNews({ data }) {
	const { title, image, url, category_url } = data

	return (
		<Link href={"/[category]/[article]"} as={`/${category_url}/${url}`}>
			<a className={style._body}>
				<div className={style._photo}>
					<Image src={image} alt={title} width={810} height={400} />
				</div>
				<div className={style._information}>
					<h4 className={style._title}>
						{title}
					</h4>
				</div>
			</a>
		</Link>
	)
}