import Image from 'next/image'
import Link from 'next/link'
import style from './mainLatestNewsItem.module.css'

export default function MainLatestNewsItem({ data }) {
	const { title, publication_date, image, url, category_url } = data
	let day, month, year

	const date = new Date(publication_date * 1000)
	day = date.getDay().toString().padStart(2, '0')
	month = (date.getMonth() + 1).toString().padStart(2, '0')
	year = date.getFullYear().toString().padStart(2, '0')

	const formattedDate = `${day}/${month}/${year}`

	return (
		<Link href={"/[category]/[article]"} as={`/${category_url}/${url}`}>
			<a className={style._body}>
				<div className={style._photo}>
					<Image src={image} alt={title} width={140} height={140} />
				</div>
				<div className={style._content}>
					<div className={style._information}>
						<div>
							{formattedDate}
						</div>
						<div>
							<h3 className={style._category}>
								{category_url}
							</h3>
						</div>
					</div>

					<h4 className={style._title}>
						{title}
					</h4>
				</div>
			</a>
		</Link>
	)
}