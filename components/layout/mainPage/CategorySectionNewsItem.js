import style from './categorySectionNeswItem.module.css'
import Image from 'next/image'
import Link from 'next/link'

export default function CategorySectionNeswItem({ data }) {
	const { title, image, publication_date, views, url, category_url } = data
	let day, month, year

	const date = new Date(publication_date * 1000)
	day = date.getDay().toString().padStart(2, '0')
	month = (date.getMonth() + 1).toString().padStart(2, '0')
	year = date.getFullYear().toString().padStart(2, '0')

	const formattedDate = `${day}/${month}/${year}`

	return (
		<Link href={"/[category]/[article]"} as={`/${category_url}/${url}`}>
			<a className={style._body}>
				<div className={style._photo}>
					<Image src={image} width={145} height={85} />
				</div>
				<div className={style._content}>
					<h3 className={style._title}>{title}</h3>
					<div className={style._statistics}>
						<div>
							<img src="/icons/clock-dark.svg" />
							{formattedDate}
						</div>
						<div>
							<img src="/icons/eye.svg" />
							{views}
						</div>
					</div>
				</div>
			</a>
		</Link>
	)
}