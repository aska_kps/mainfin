import style from './mainLatestNewsBlock.module.css'
import Link from 'next/link'
import MainLatestNewsItem from './MainLatestNewsItem'
import { useTranslation } from 'next-i18next'

export default function MainLatestNewsBlock({ data }) {
	const { t } = useTranslation('common')

	return (
		<div className={style._body}>
			<div className={style._header}>
				<p className={style._title}>
					{t("latestNewsLabel")}
				</p>
				{/* <Link href={"/[category]/[article]"} as={`/${category}/${news}`}>
					<a>
						{t("allNewsLabel")} <img src="/icons/ellipse-arrow.svg" alt={t("allNewsLabel")} />
					</a>
				</Link> */}
			</div>
			<div className="row">
				{data.map(item => (
					<div className="col-sm-6 col-lg-12 py-lg-0 py-3" key={item.id}>
						<MainLatestNewsItem data={item} />
					</div>
				))}
			</div>
		</div>
	)
}