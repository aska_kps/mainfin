import PopularNewsItem from './PopularNewsItem'
import style from './popularNewsBlock.module.css'
import { useTranslation } from 'next-i18next'

export default function PopularNewsBlock({ news }) {
	const { t } = useTranslation('common')

	return (
		<div className={style._body}>
			<div className={style._header}>
				<p>{t("popularLabel")}</p>
			</div>
			<div className="row">
				{news.map(item => (
					<div className="col-md-6 col-lg-12" key={item.id}>
						<PopularNewsItem data={item} />
					</div>
				))}
			</div>
		</div>
	)
}