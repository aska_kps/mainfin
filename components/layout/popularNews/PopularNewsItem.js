import Link from 'next/link'
import Image from 'next/image'
import style from './popularNewsItem.module.css'

export default function popularNewsItem({ data }) {
	const { id, title, views, publication_date, image, url, category_url } = data
	let day, month, year

	const date = new Date(publication_date * 1000)
	day = date.getDay().toString().padStart(2, '0')
	month = (date.getMonth() + 1).toString().padStart(2, '0')
	year = date.getFullYear().toString().padStart(2, '0')

	const formattedDate = `${day}/${month}/${year}`

	return (
		<div className={style._item}>
			<div className={style._statistics}>
				<div>{formattedDate}</div>
				<div>
					<Image src="/icons/eye.svg" width={15} height={10} />
					{views}
				</div>
			</div>
			<div className={style._information}>
				<Link href={"/[category]/[article]"} as={`/${category_url}/${url}`}>
					<a>
						<div className={style._photo}>
							<Image src={image} width={40} height={40} alt={title} />
						</div>
						<div className={style._title}>{title}</div>
					</a>
				</Link>
			</div>
		</div>
	)
}