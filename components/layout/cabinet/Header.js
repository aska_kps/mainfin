import style from './header.module.css'
import Link from 'next/link'
import User from './User'
import { useRouter } from 'next/router'
import { signOut } from 'next-auth/client'

export default function Header({ user }) {
	const router = useRouter()
	const pathname = router.pathname

	return (
		<header className={style._body}>
			<div className="container">
				<div className="row justify-content-between">
					<div className="col-6 col-md-3 col-lg-4 mr-auto d-flex align-items-center order-1">
						<Link href="/">
							<a>
								<img src="/icons/logo.svg" alt="Mainfin" title="Mainfin" />
							</a>
						</Link>
					</div>

					<div className="col-10 col-md-4 col-xl-5 mt-4 mt-md-0 d-flex align-items-center order-3 order-md-2">
						<div className={style._search}>
							<input type="text" name="search" placeholder="Поиск" />
							<button type="button">
								<img src="/icons/search.svg" alt="Поиск" />
							</button>
						</div>
					</div>

					{pathname == "/cabinet/create" &&
						<div className="col-6 col-md-3 col-xl-2 d-flex justify-content-end order-2 order-md-3">
							<User data={user} />
						</div>
					}

					{pathname == "/cabinet/edit" &&
						<div className="col-6 col-md-3 col-xl-2 d-flex justify-content-end order-2 order-md-3">
							<User data={user} />
						</div>
					}

					<div className="col-2 col-md-1 mt-4 mt-md-0 order-4">
						<div className={style._logout}>
							<img src="/icons/logout.svg" alt="Выход" title="Выход" onClick={signOut} />
						</div>
					</div>
				</div>
			</div>
		</header >
	)
}