import style from './userInfo.module.css'
import React, { useState } from 'react'
import UserEditing from './UserEditing'

export default function UserInfo({ data }) {
	const [userData, setUserData] = useState(data)
	const [editingVisibility, setEditingVisibility] = useState(false)

	const { name, surname, position, avatar, country, city, countries, cities, socials } = userData
	const { email, facebook, twitter, instagram } = socials

	const handleEditingVisibility = (value) => {
		setEditingVisibility(value)
		document.body.classList.toggle('lock')
	}

	return (
		<div>
			<div className={style._banner}>
				<p className={style._subtitle}>Личный кабинет пользователя</p>
				<p className={style._title}>{name}, добро пожаловать! </p>
			</div>
			<div className="row justify-content-between">
				<div className="col-md-4 col-xl-3">
					<div className={`${style._photo} mx-auto ms-lg-4`}>
						<img src={avatar} alt={name} />
					</div>
				</div>
				<div className="col-md-8 col-xl-9">
					<div className={style._information}>
						<div className={style._information_top}>
							<div>
								<p className={style._username}>{`${surname} ${name}`}</p>
								<p>{position}</p>
							</div>
							<div className="d-flex justify-content-between">
								<p>
									<img src="/icons/notifications.svg" alt="Уведомления" title="Уведомления" />
								</p>
								<p style={{ cursor: "pointer" }}
									onClick={() => {
										handleEditingVisibility(true)
									}}
								>
									<img src="/icons/edit.svg" alt="Редактировать" title="Редактировать" />
									Редактировать
								</p>
							</div>
						</div>
						<div className="row">
							<div className="w-auto mr-2">
								<img src="/icons/location.svg" alt="icon" />
								{countries[country]} {cities[city]}
							</div>
							<div className="w-auto">
								<ul className={style._socials}>
									{email &&
										<li>
											<img src="/icons/facebook-dark.svg" alt="icon" />
											{email}
										</li>
									}

									{facebook &&
										<li>
											<img src="/icons/facebook-dark.svg" alt="icon" />
											{facebook}
										</li>
									}

									{twitter &&
										<li>
											<img src="/icons/twitter-dark.svg" alt="icon" />
											{twitter}
										</li>
									}

									{instagram &&
										<li>
											<img src="/icons/instagram-dark.svg" alt="icon" />
											{instagram}
										</li>
									}
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

			<UserEditing
				data={userData}
				handleData={setUserData}
				visibility={editingVisibility}
				handleVisibility={handleEditingVisibility}
			/>
		</div >
	)
}
