import style from './user.module.css'
import Link from 'next/link'

export default function User({ data }) {
	const { name, position, avatar } = data

	return (
		<Link href="/cabinet/my">
			<a>
				<div className={style._body}>
					<div className={style._information}>
						<p className={style._name}>
							{name}
						</p>
						<p className={style._position}>
							{position}
						</p>
					</div>
					<div className={style._photo}>
						<img src={avatar} alt={name} />
					</div>
				</div>
			</a>
		</Link>
	)
}