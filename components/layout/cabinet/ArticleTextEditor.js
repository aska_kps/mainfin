import React, { useState } from 'react'
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { EditorState, ContentState, convertToRaw, convertFromHTML } from 'draft-js'
import draftToHtml from 'draftjs-to-html';
import dynamic from 'next/dynamic'
const Editor = dynamic(
	() => import('react-draft-wysiwyg').then(mod => mod.Editor),
	{ ssr: false }
)

function uploadImageCallBack(file) {
	return new Promise(
		(resolve, reject) => {
			const reader = new FileReader();
			reader.onload = e => resolve({ data: { link: e.target.result } });
			reader.onerror = e => reject(e);
			reader.readAsDataURL(file);
		});
}

export default function ArticleTextEditor({ data, handleContent }) {
	const [editorState, setEditorState] = useState(EditorState.createWithContent(ContentState.createFromBlockArray(convertFromHTML(data))))

	const onEditorStateChange = (editorState) => {
		setEditorState(editorState)
		handleContent(draftToHtml(convertToRaw(editorState.getCurrentContent())))
	}

	return (
		<div>
			<Editor
				editorState={editorState}
				toolbarClassName="toolbarClassName"
				wrapperClassName="wrapperClassName"
				editorClassName="editorClassName"
				onEditorStateChange={onEditorStateChange}
				placeholder="Начните печатать..."
				required
				toolbar={{
					image: {
						uploadCallback: uploadImageCallBack,
						previewImage: true,
						alt: { present: true, mandatory: true },
					},
				}}
			/>
			<style jsx global>{`
				.rdw-editor-wrapper {
					box-shadow: 0px 2px 10px rgba(154, 154, 154, 0.25);
					margin-bottom: 50px;
				}
				.rdw-editor-toolbar {
					padding: 20px;
					border-bottom: 2px solid #01C71B;
				}
				.rdw-editor-main {
					padding: 20px;
					min-height: 200px;
					max-height: 1000px;
				}
			`}</style>
		</div>
	)
}
