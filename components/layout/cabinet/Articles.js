import Link from 'next/link'
import style from './articles.module.css'
import ArticleEntry from './ArticleEntry'
import Loader from '/components/layout/Loader'
import React, { useState } from 'react'
import CabinetService from '/components/services/CabinetService'
import { useSession } from 'next-auth/client'

export default function Articles({ data, statuses }) {
	const [articles, setArticles] = useState(data)
	const [loader, setLoader] = useState(false)
	const [active, setActive] = useState({
		all: style._navigationItem_active,
	})
	const [session] = useSession()

	// articles.forEach(article => {
	// 	console.log(article.status)
	// })

	const [articlesCounter, setArticleCounter] = useState({
		published: 0,
		moderation: 0,
		rejected: 0,
	})

	const changeArticlesHandler = async (request) => {
		setLoader(true)

		const cs = new CabinetService(session)
		const data = await cs.getAuthorArticles(request)

		setActive({
			[request]: style._navigationItem_active,
		})

		setArticles(data)
		setLoader(false)
	}

	return (
		<div className={style._wrapper}>
			<div className={`${style._header} row justify-content-between`}>
				<div className="col-md-7 col-lg-7 order-2 order-md-1">
					<div className="row">
						<div className={`${style._navigationItem} ${active.all} col`}>
							<p onClick={() => { changeArticlesHandler('all') }}>Все статьи</p>
						</div>

						<div className={`${style._navigationItem} ${active.draft} col`}>
							<p onClick={() => { changeArticlesHandler('draft') }}>Черновик</p>
						</div>

						<div className={`${style._navigationItem} ${active.archive} col`}>
							<p onClick={() => { changeArticlesHandler('archive') }}>В архиве</p>
						</div>
					</div>
				</div>

				<div className="col-md-5 col-lg-3 order-1 order-md-2">
					<div className={`${style._buttons} row`}>
						<div className="col">
							<Link href="/cabinet/create">
								<a className={`${style._button} ${style._buttonAdd}`}>
									Добавить
								</a>
							</Link>
						</div>

						<div className="col">
							<button type="button" className={`${style._button} ${style._buttonFilter}`}>
								<img src="/icons/filter.svg" />
								Фильтр
							</button>
						</div>
					</div>
				</div>
			</div>

			{articles && (
				<div className={style._body}>
					{loader && <Loader />}

					{articles.map(item => (
						< div key={item.id} >
							<ArticleEntry data={item} statuses={statuses} />
						</div>
					))}
				</div>
			)
			}

			<div className={`${style._bottom} row`}>
				<div className="col-sm py-3 py-sm-0">Всего опубликовано: {articlesCounter.published}</div>
				<div className="col-sm py-3 py-sm-0">Всего в ожидании: {articlesCounter.moderation}</div>
				<div className="col-sm py-3 py-sm-0">Всего отказано: {articlesCounter.rejected}</div>
			</div>
		</div >
	)
}