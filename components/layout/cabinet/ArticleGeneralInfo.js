import DatePicker from "react-datepicker"
import 'react-datepicker/dist/react-datepicker.css'
import style from './articleGeneralInfo.module.css'
import React, { useState } from 'react'
import imageToBase64 from 'image-to-base64/browser'
import Select from 'react-select'

export default function ArticleGeneralInfo({ title, date, poster, language, category, languages, categories, description, handleTitle, handleDate, handlePoster, handleLanguage, handleCategory, handleDescription }) {
	const [profileImage, setProfileImage] = useState(poster)

	const changeProfileImageHandler = (e) => {
		if (e.target.files[0]) {
			const image = URL.createObjectURL(e.target.files[0])
			setProfileImage(image)

			imageToBase64(image)
				.then(
					(response) => {
						handlePoster(response)
					}
				)
				.catch(
					(error) => {
						console.log(error)
					}
				)
		}
	}

	const languageOptions = []
	Object.keys(languages).map(languageId => (
		languageOptions.push({ value: languageId, label: languages[languageId] })
	))

	const categoryOptions = []
	Object.keys(categories).map(categoryId => (
		categoryOptions.push({ value: categoryId, label: categories[categoryId] })
	))

	const selectTheme = (theme) => {
		// console.log(theme)

		return {
			...theme,
			colors: {
				...theme.colors,
				primary: '#4E4963',
				primary25: '#898597',
				primary50: '#B0ADB9',
			},
			spacing: {
				...theme.spacing,
				baseUnit: 8,
				menuGutter: 4,
			}

		}
	}

	const selectStyles = {
		control: base => ({
			...base,
			border: 0,
			boxShadow: "none"
		})
	};

	return (
		<div className="row mb-5 justify-content-center">
			<div className="col-md-2 mb-4 mb-md-0">
				<div className={style._poster}>
					<img src={profileImage} />
					<div className={style._profilePhotoInputField}>
						<input className={style._profilePhotoInput}
							type="file"
							onChange={e => {
								changeProfileImageHandler(e)
							}}
						/>
					</div>
				</div>
			</div>

			<div className="col-md-10">
				<div className="row">
					<div className="col-12">
						<div className={style._inputField}>
							<input name="title"
								type="text"
								placeholder="Заголовок статьи"
								required
								defaultValue={title}
								onChange={(e) => {
									handleTitle(e.target.value)
								}}
							/>
						</div>
					</div>

					<div className="col-sm-6 col-md-4 mt-4">
						<div className={`${style._inputField} ${style._inputField_date}`}>
							<DatePicker selected={date}
								onChange={(date) => {
									handleDate(date)
								}}
								dateFormat="dd/MM/yy"
								name="date"
							/>
							<img src="/icons/calendar.svg" alt="icon" />
						</div>
					</div>

					<div className="col-sm-6 col-md-4 mt-4">
						<div className={style._inputField}>
							<Select options={languageOptions}
								defaultValue={{ label: languages[language], value: language }}
								onChange={(obj) => { handleLanguage(obj.value) }}
								theme={selectTheme}
								styles={selectStyles}
								placeholder="Языки"
							/>
						</div>
					</div>

					<div className="col-md-4 mt-4">
						<div className={style._inputField}>
							<Select options={categoryOptions}
								defaultValue={{ label: categories[category], value: category }}
								onChange={(obj) => { handleCategory(obj.value) }}
								theme={selectTheme}
								styles={selectStyles}
								placeholder="Категории"
							/>
						</div>
					</div>
				</div>
			</div>

			<div className="pt-5">
				<div className={style._inputField}>
					<textarea name="description"
						placeholder="Короткое описание"
						defaultValue={description}
						required
						onChange={(e) => {
							handleDescription(e.target.value)
						}}>
					</textarea>
				</div>
			</div>

		</div>
	)
}