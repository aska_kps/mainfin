import style from './articleEntry.module.css'
import Link from 'next/link'

export default function ArticleEntry({ data, statuses }) {
	const { id, title, created_at, status, url } = data

	const fullDate = new Date(created_at * 1000)
	const date = `${fullDate.getDate().toString().padStart(2, '0')}/${(fullDate.getMonth() + 1).toString().padStart(2, '0')}/${fullDate.getFullYear().toString().padStart(4, '0')}`

	const textStatuses = JSON.parse(process.env.NEXT_PUBLIC_TEXT_STATUSES)

	return (
		<div className={`${style._body} row`}>
			<div className={`${style._item} col`}>
				{date}
			</div>
			<div className={`${style._item} col`}>
				{title}
			</div>
			<div className={`${style._item} ${textStatuses[status]} col`}>
				{statuses[status]}
			</div>
			<div className={`${style._item} col-1 col-md`}>
				<a href={`/${url}`} target="_blank">
					<span className="d-none d-md-inline">Ссылка на публикацию</span>
					<img src="/icons/link.svg" alt="Иконка" title="Редактировать" className="d-inline d-md-none" style={{ width: "18px", height: "18px" }} />
				</a>
			</div>
			<div className={`${style._item} col-1 col-md`}>
				<Link href={`/cabinet/edit?id=${id}`}>
					<a>
						<span className="d-none d-md-inline">Редактировать</span>
						<img src="/icons/edit.svg" alt="Иконка" title="Редактировать" className="d-inline d-md-none" style={{ width: "18px", height: "18px" }} />
					</a>
				</Link>
			</div>
		</div >
	)
}