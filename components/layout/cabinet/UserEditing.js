import style from './userEditing.module.css'
import React, { useState } from 'react'
import { useSession } from 'next-auth/client'
import imageToBase64 from 'image-to-base64/browser'
import CabinetService from '/components/services/CabinetService'
import Select from 'react-select'

export default function UserEditing({ data, handleData, visibility, handleVisibility }) {
	const { name, surname, position, avatar, country, city, countries, cities, socials } = data
	const [session] = useSession()

	const [userName, setUserName] = useState(name)
	const [userSurname, setUserSurname] = useState(surname)
	const [userPosition, setUserPosition] = useState(position)
	const [userAvatar, setUserAvatar] = useState(avatar)
	const [userCountry, setUserCountry] = useState(country)
	const [userCity, setUserCity] = useState(city)
	const [userSocials, setUserSocials] = useState(socials)
	const [profileImageBase64, setProfileImageBase64] = useState(avatar) // передать изображение в base64

	const countryOptions = []
	Object.keys(countries).map(countryId => (
		countryOptions.push({ value: countryId, label: countries[countryId] })
	))

	const cityOptions = []
	Object.keys(cities).map(cityId => (
		cityOptions.push({ value: cityId, label: cities[cityId] })
	))

	const toggler = (e) => {
		const input = e.target.previousElementSibling
		input.removeAttribute("readonly")
		input.focus()
	}

	const [notification, setNotification] = useState({
		text: '',
		status: '',
		visibility: false
	})

	const changeProfileImageHandler = (e) => {
		console.log(e.target.files[0])
		if (e.target.files[0]) {
			const image = URL.createObjectURL(e.target.files[0])
			setUserAvatar(image)
			imageToBase64(image)
				.then(
					(response) => {
						setProfileImageBase64(response)
					}
				)
				.catch(
					(error) => {
						console.log(error)
					}
				)
		}
	}

	const setDefaultProfileImageHandler = (image) => {
		setUserAvatar(image)

		imageToBase64(image)
			.then(
				(response) => {
					setProfileImageBase64(response)
				}
			)
			.catch(
				(error) => {
					console.log(error)
				}
			)
	}

	const handleSubmit = async (event) => {
		event.preventDefault()

		const passData = {
			name: userName,
			surname: userSurname,
			// position: userPosition,
			country: userCountry,
			city: userCity,
			socials: userSocials,
			avatar: profileImageBase64,
		}

		const cs = new CabinetService(session)
		const response = await cs.updateAuthorInformation(passData)

		if (response) {
			handleData({
				...data,
				name: passData.name,
				surname: passData.surname,
				country: passData.country,
				city: passData.city,
				socials: passData.socials,
				avatar: passData.avatar,
			})

			setNotification({
				text: 'Изменения сохранены.',
				status: 'success',
				visibility: true
			})

			setTimeout(() => {
				setNotification({
					visibility: false
				});
			}, 2000)

		} else {
			setNotification({
				text: 'Не удалось внести изменения, повторите позже.',
				status: 'error',
				visibility: true
			})

			setTimeout(() => {
				setNotification({
					visibility: false
				});
			}, 2000)
		}
	}

	const selectTheme = (theme) => {
		// console.log(theme)

		return {
			...theme,
			colors: {
				...theme.colors,
				primary: '#4E4963',
				primary25: '#898597',
				primary50: '#B0ADB9',
			},
			spacing: {
				...theme.spacing,
				baseUnit: 4,
				menuGutter: 4,
			}

		}
	}

	const selectStyles = {
		control: base => ({
			...base,
			border: 0,
			boxShadow: "none"
		})
	};

	return (
		<div className={`${style._wrapper} ${visibility ? style._visible : style._hidden}`}>
			{notification.visibility && notification.text && (
				<p className={`notification ${notification.status}`}>{notification.text}</p>
			)}

			<form onSubmit={handleSubmit} className={style._form} method="POST">
				<div className={style._header}>
					<p>Редактировать профиль</p>
					<p style={{ cursor: "pointer" }}
						onClick={() => {
							handleVisibility(false)
						}}
					>
						&times;
					</p>
				</div>

				<div className={`${style._body} row`}>
					<div className="col-sm-4">
						<div className={style._profilePhotoPicker}>
							<div className={`${style._photo} mx-auto`}>
								<img src={userAvatar} />
								<div className={style._profilePhotoInputField}>
									<input className={style._profilePhotoInput} type="file" onChange={e => changeProfileImageHandler(e)} />
								</div>
							</div>
							<div className={style._profilePhotoRemove} onClick={() => setDefaultProfileImageHandler('/icons/defaultImage.svg')}>Удалить</div>
						</div>
					</div>

					<div className="col-sm-8">
						<div className="row pb-5">
							<div className="col-6">
								<div className={style._textInputField}>
									<label htmlFor="surname">Фамилия:</label> <br />
									<input id="surname"
										name="surname"
										type="text"
										minLength="2"
										maxLength="20"
										defaultValue={userSurname} readOnly
										onChange={(e) => {
											setUserSurname(e.target.value)
										}}
									/>
									<img src="/icons/edit-pen.svg" alt="icon" onClick={toggler} />
								</div>
							</div>

							<div className="col-6">
								<div className={style._textInputField}>
									<label htmlFor="country">Страна:</label> <br />
									<Select options={countryOptions}
										defaultValue={{ label: countries[userCountry], value: userCountry }}
										onChange={(obj) => { setUserCountry(obj.value) }}
										theme={selectTheme}
										styles={selectStyles}
										placeholder="Страна"
									/>
								</div>
							</div>

							<div className="col-6">
								<div className={style._textInputField}>
									<label htmlFor="name">Имя:</label> <br />
									<input id="name"
										name="name"
										type="text"
										minLength="2"
										maxLength="20"
										defaultValue={userName}
										readOnly
										onChange={(e) => {
											setUserName(e.target.value)
										}}
									/>
									<img src="/icons/edit-pen.svg" alt="icon" onClick={toggler} />
								</div>
							</div>

							<div className="col-6">
								<div className={style._textInputField}>
									<label htmlFor="city">Город:</label> <br />
									<Select options={cityOptions}
										defaultValue={{ label: cities[userCity], value: userCity }}
										onChange={(obj) => { setUserCity(obj.value) }}
										theme={selectTheme}
										styles={selectStyles}
										placeholder="Город"
									/>
								</div>
							</div>

							<div className="col-6">
								<div className={style._textInputField}>
									<label htmlFor="position">Должность:</label> <br />
									<input id="position"
										name="position"
										type="text"
										minLength="2"
										maxLength="20"
										defaultValue={userPosition}
										readOnly
										onChange={(e) => {
											setUserPosition(e.target.value)
										}}
									/>
									<img src="/icons/edit-pen.svg" alt="icon" onClick={toggler} />
								</div>
							</div>

							<div className="col-6">
								<div className={style._textInputField}>
									<label htmlFor="email">E-mail:</label> <br />
									<input id="email"
										name="email"
										type="email"
										minLength="2"
										maxLength="20"
										defaultValue={userSocials.email}
										readOnly
										onChange={(e) => {
											setUserSocials({
												...userSocials,
												email: e.target.value
											})
										}}
									/>
									<img src="/icons/edit-pen.svg" alt="icon" onClick={toggler} />
								</div>
							</div>
						</div>

						<div className="row">
							<div className="col-6">
								<div className={style._textInputField}>
									<label htmlFor="twitter">Twitter:</label> <br />
									<input id="twitter"
										name="twitter"
										type="text"
										minLength="2"
										maxLength="20"
										defaultValue={userSocials.twitter}
										readOnly
										onChange={(e) => {
											setUserSocials({
												...userSocials,
												twitter: e.target.value
											})
										}}
									/>
									<img src="/icons/edit-twitter.svg" alt="icon" onClick={toggler} />
								</div>
							</div>

							<div className="col-6">
								<div className={style._textInputField}>
									<label htmlFor="facebook">Facebook:</label> <br />
									<input id="facebook"
										name="facebook"
										type="text"
										minLength="2"
										maxLength="20"
										defaultValue={userSocials.facebook}
										readOnly
										onChange={(e) => {
											setUserSocials({
												...userSocials,
												facebook: e.target.value
											})
										}}
									/>
									<img src="/icons/edit-facebook.svg" alt="icon" onClick={toggler} />
								</div>
							</div>

							<div className="col-6">
								<div className={style._textInputField}>
									<label htmlFor="twitter">Instagram::</label> <br />
									<input id="instagram"
										name="instagram"
										type="text"
										minLength="2"
										maxLength="20"
										defaultValue={userSocials.instagram}
										readOnly
										onChange={(e) => {
											setUserSocials({
												...userSocials,
												instagram: e.target.value
											})
										}}
									/>
									<img src="/icons/edit-instagram.svg" alt="icon" onClick={toggler} />
								</div>
							</div>
						</div>
					</div>
				</div>

				<div className={style._footer}>
					<button className={style._button} type="submit">Сохранить</button>
				</div>
			</form>
		</div >
	)
}