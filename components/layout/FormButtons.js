import style from './formButtons.module.css'

export default function FormButtons({ status, handleStatus }) {
	const disabled = status === 0 ? false : true // if status in draft

	return (
		<div className="row justify-content-between justify-content-md-end">
			<div className="w-auto">
				<button type="submit"
					// disabled={disabled}
					className={`${style._button} ${style._buttonSave}`}
					onClick={() => {
						handleStatus(0)
					}}>
					Сохранить
				</button>
			</div>

			<div className="w-auto">
				<button type="submit"
					// disabled={disabled}
					className={`${style._button} ${style._buttonPublish}`}
					onClick={() => {
						handleStatus(1)
					}}>
					Опубликовать
				</button>
			</div>
		</div>
	)
}