import styles from '/styles/author.module.css'
import Link from 'next/link'

export default function Author({ data }) {
	const { id, name, surname, avatar, position } = data

	return (
		<div className={styles._wrapper}>

			<div className={styles._body}>
				<Link href={`/user-${id}`}>
					<a>
						<div className={`${styles._content} px-2 px-lg-4`}>
							<div className={styles._photo}>
								<img src={avatar} />
							</div>
							<div className={styles._information}>
								<p className={styles._title}>{name} {surname}</p>
								<p className={styles._text}>{position}</p>
							</div>
						</div>
					</a>
				</Link>
				<div className={styles._share}>
					<p>Поделиться:</p>
					<div className={styles._links}>
						<img src="/icons/vk-share.svg" />
						<img src="/icons/telegram-share.svg" />
					</div>
				</div>
			</div>
		</div>
	)
}