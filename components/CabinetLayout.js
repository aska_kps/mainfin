import Head from 'next/head'
import Header from '/components/layout/cabinet/Header'
import { useSession } from 'next-auth/client'

export default function CabinetLayout({ children, title = 'title' }) {
	const [session] = useSession()
	const { user } = session
	user.position = "Журналист"

	return (
		<>
			<Head>
				<title>{title}</title>
				<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
				<meta charSet="utf-8" />
				<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
			</Head>
			
			<Header user={user} />
			

			<div className="container">
				<div className="row">
					{children}
				</div>
			</div>
		</>
	)
}