import axios from 'axios'

export default class CabinetService {
	constructor(session = null) {
		this.session = session
		this.apiUrl = process.env.NEXT_PUBLIC_CABINET_API_URL
	}

	async request(url, options = null) {
		const init = {
			headers: {
				'Authorization': `Bearer ${this.session.user.token}`,
				'Content-Type': 'application/json',
			},
		}

		Object.assign(init, options)

		const res = await fetch(this.apiUrl + url, init)

		if (res.status == 500) {
			console.log('Error 500')
			return false
		}

		if (res.status === 401) {
			console.log('Error 401')
			return false
		}

		if (!res.ok)
			throw res;

		return await res.json();
	}

	async login(credentials) {
		return await axios.post(this.apiUrl + 'login', credentials)
	}

	async getAuthorArticles(request) {
		return this.request(`articles/${request}`)
	}

	async getAuthorArticle(id) {
		return this.request(`article/${id}`)
	}

	async getAuthor() {
		return this.request('getuser')
	}

	async updateAuthorArticle(id, data) {
		const options = {
			method: 'POST',
			body: JSON.stringify(data),
			credentials: 'include',
		}

		return this.request(`update/${id}`, options, data)
	}

	async createAuthorArticle(data) {
		const options = {
			method: 'POST',
			body: JSON.stringify(data),
			credentials: 'include',
		}

		return this.request('create', options, data)
	}

	async getCommonData() {
		return this.request('getcommondata')
	}

	async updateAuthorInformation(data) {
		const options = {
			method: 'POST',
			body: JSON.stringify(data),
			credentials: 'include',
		}

		return this.request('updateuser', options, data)
	}

	async getMyPage(type) {
		return this.request(`getmypage/${type}`)
	}

}