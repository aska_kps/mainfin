export default class SiteService {
	constructor(locale = null) {
		this.locale = locale
		this.apiUrl = process.env.NEXT_PUBLIC_SITE_API_URL
	}

	async request(url, options = null) {
		const init = {
		}

		Object.assign(init, options)
		const response = await fetch(this.apiUrl + url, init)

		if (response.status == 500) {
			console.log('Error 500')
			// return false
		}

		if (response.status === 401) {
			console.log('Error 401')
			// return false
		}

		if (!response.ok)
			throw response;

		const dd = await response.json()

		return dd;
	}

	async getGeneral() {
		return this.request(`${this.locale}/general`)
	}

	async getHomePage() {
		return this.request(`${this.locale}`)
	}

	async getCategoryPage(category) {
		return this.request(`${this.locale}/${category}`)
	}

	async getArticlePage(category, article) {
		return this.request(`${this.locale}/${category}/${article}`)
	}
}