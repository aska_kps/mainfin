import NextAuth from 'next-auth'
import Providers from 'next-auth/providers'
import CabinetService from '/components/services/CabinetService'

const options = {
	providers: [
		Providers.Credentials({
			name: "Credentials",
			credentials: {
				username: { label: "Email", type: "text", placeholder: "example@mail.ru" },
				password: { label: "Password", type: "password" }
			},
			async authorize(credentials) {
				const cabinetService = new CabinetService()
				const response = await cabinetService.login(credentials)

				if (response) {
					return response.data
				} else {
					return null
				}
			}
		})
	],
	session: {
		jwt: true,
		maxAge: 30 * 24 * 60 * 60, // 30 days
		// updateAge: 24 * 60 * 60, // 24 hours, Note: This option is ignored if using JSON Web Tokens
	},
	pages: {
		signIn: '/cabinet/login',
		//   signOut: '/auth/sinout',
		//   errow: '/auth/error',
		//   verifyRequest: '/auth/verify-request'
	},
	callbacks: {
		jwt: async (token, user, account, profile, isNewUser) => {
			//  "user" parameter is the object received from "authorize"
			//  "token" is being send below to "session" callback...
			//  ...so we set "user" param of "token" to object from "authorize"...
			//  ...and return it...
			user && (token.user = user);
			return Promise.resolve(token)   // ...here
		},
		session: async (session, user, sessionToken) => {
			//  "session" is current session object
			//  below we set "user" param of "session" to value received from "jwt" callback
			session.user = user.user;
			return Promise.resolve(session);
		}
	}
}

const Auth = (req, res) => NextAuth(req, res, options)

export default Auth