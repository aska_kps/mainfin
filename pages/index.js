import Layout from '/components/Layout'
import CategorySection from '/components/layout/mainPage/CategorySection'
import LatestSection from '/components/layout/LatestSection'
import MainLatestNewsBlock from '/components/layout/mainPage/MainLatestNewsBlock'
import MainNews from '/components/layout/mainPage/MainNews'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import SiteService from '/components/services/SiteService'

export default function Index({ data, generalData }) {
	const { mainArticle, latestArticles, categories } = data
	const latestFirstPart = latestArticles.slice(0, 4)
	const latestSecondPart = latestArticles.slice(4)

	return (
		<Layout title={'MainFin | Новостной портал'} generalData={generalData}>
			<main>
				<div className="row pb-5">
					<div className="col-lg-8">
						<MainNews data={mainArticle} />
					</div>

					{latestFirstPart && (
						<div className="col-lg-4">
							<MainLatestNewsBlock data={latestFirstPart} />
						</div>
					)}
				</div>

				<LatestSection news={latestSecondPart} />

				{categories.map(category => (
					<div key={category.id}>
						<CategorySection category={category} />
					</div>
				))}
			</main>
		</Layout>
	)
}

export async function getServerSideProps(context) {
	const { locale } = context

	const ss = new SiteService(locale)
	const data = await ss.getHomePage()

	return {
		props: {
			...(await serverSideTranslations(locale, ['common'])),
			data,
		}
	}
}