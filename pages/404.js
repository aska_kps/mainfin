import Layout from '/components/Layout'
import Link from 'next/link'
import styles from '/styles/notFound.module.css'
// import { serverSideTranslations } from 'next-i18next/serverSideTranslations'

export default function NotFound({ generalData }) {
	return (
		<Layout title={"404"} generalData={generalData}>
			<div className={styles._body}>
				<div className={styles._error}>
					<p>Страница не найдена</p>
					<p>Ошибка <span>404</span></p>
				</div>

				<Link href="/">
					<a className={styles._link}>
						Вернуться на главную страницу
					</a>
				</Link>
			</div>
		</Layout>
	)
}

// export async function getServerSideProps(context) {
// 	const { locale } = context

// 	return {
// 		props: {
// 			...(await serverSideTranslations(locale, ['common'])),
// 		}
// 	}
// }