import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/globals.css'
import { Provider } from 'next-auth/client'
import { appWithTranslation } from 'next-i18next'
import React, { useState } from 'react'
import SiteService from '/components/services/SiteService'

const App = ({ Component, pageProps, generalData }) => {
	const [hookData, setHookData] = useState(false)

	if (!hookData && Object.keys(generalData).length) {
		setHookData(generalData)
	}

	pageProps.generalData = hookData

	return (
		<Provider session={pageProps.session}
			options={{
				clientMaxAge: 30
			}}
		>
			<Component {...pageProps} />
		</Provider>
	)
}

App.getInitialProps = async (context) => {
	const pageProps = {}
	const { locale } = context.ctx
	const path = context.router.asPath

	if (!path.includes('cabinet')) {
		const ss = new SiteService(locale)
		const generalData = await ss.getGeneral()

		return {
			pageProps,
			generalData,
		}
	}

	const generalData = {}

	return {
		pageProps,
		generalData,
	}
}

export default appWithTranslation(App)
