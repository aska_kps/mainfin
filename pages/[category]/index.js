import Layout from '/components/Layout'
import PopularNewsBlock from '/components/layout/popularNews/PopularNewsBlock'
import CategoryNewsItem from '/components/layout/category/CategoryNewsItem'
import CategoryNewsMain from '/components/layout/category/CategoryNewsMain'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import SiteService from '/components/services/SiteService'

export default function Category({ data, generalData }) {
	const { category, mainArticle, latestArticles, popularArticles } = data
	const { title, seo_title, seo_description } = category

	return (
		<Layout title={seo_title} description={seo_description} generalData={generalData}>
			<main className="col-lg-9">
				{mainArticle && (
					<div>
						<CategoryNewsMain data={mainArticle} />
					</div>
				)}

				{latestArticles && (
					<div className="row">
						{latestArticles.map(item => (
							<div className="col-md-6 col-lg-4" key={item.id}
								style={{ paddingTop: "15px", paddingBottom: "15px" }}>
								<CategoryNewsItem data={item} />
							</div>
						))}
					</div>
				)}
			</main>

			{popularArticles && (
				<aside className="col-lg-3">
					<PopularNewsBlock news={popularArticles} />
				</aside>
			)}
		</Layout>
	)
}

export async function getServerSideProps(context) {
	const { locale } = context
	const { category } = context.query

	const ss = new SiteService(locale)
	const data = await ss.getCategoryPage(category)

	if (!Object.keys(data).length) {
		return {
			notFound: true,
		}
	}

	return {
		props: {
			...(await serverSideTranslations(locale, ['common'])),
			data,
		}
	}
}