import Layout from '/components/Layout'
import NewsBody from '/components/layout/NewsBody'
import LatestNewsBlock from '/components/layout/latestNews/LatestNewsBlock'
import PopularNewsBlock from '/components/layout/popularNews/PopularNewsBlock'
import Author from '/components/layout/Author'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import SiteService from '/components/services/SiteService'

export default function News({ data, generalData }) {
	const { latestArticles, popularArticles, article, author } = data
	const { title } = article
	author.position = 'журналист'

	return (
		<Layout title={title} description={title} generalData={generalData}>
			<aside className="col-lg-3 order-2 order-lg-1 mb-5 mb-lg-0">
				<LatestNewsBlock news={latestArticles} />
			</aside>

			<main className="col-lg-6 order-1 order-lg-2">
				<NewsBody data={article} />

				<Author data={author} />
			</main>

			<aside className="col-lg-3 order-3 mb-5 mb-lg-0">
				<PopularNewsBlock news={popularArticles} />
			</aside>
		</Layout>
	)
}

export async function getServerSideProps(context) {
	const { locale } = context
	const { category, article } = context.query

	const ss = new SiteService(locale)
	const data = await ss.getArticlePage(category, article)

	if (!data.article) {
		return {
			notFound: true
		}
	}

	return {
		props: {
			...(await serverSideTranslations(locale, ['common'])),
			data,
		}
	}
}