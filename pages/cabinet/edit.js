import CabinetLayout from '/components/CabinetLayout'
import CabinetService from '/components/services/CabinetService'
import ArticleTextEditor from '/components/layout/cabinet/ArticleTextEditor'
import ArticleGeneralInfo from '/components/layout/cabinet/ArticleGeneralInfo'
import FormButtons from '/components/layout/FormButtons'
import { useSession, signIn, getSession } from 'next-auth/client'
import { useState } from 'react'
import { useRouter } from 'next/router'

export default function Edit({ data }) {
	const [session, loading] = useSession()

	if (loading) {
		return null
	}

	if (session) {
		const [notification, setNotification] = useState({
			text: '',
			status: '',
			visibility: false
		})

		const { title, publication_date, status, image, short_content, content, language, category_id, languages, categories } = data
		const defaultPoster = '/images/poster.jpg'

		const [articleTitle, setArticleTitle] = useState(title)
		const [articleDate, setArticleDate] = useState(publication_date || Date.parse(new Date) / 1000)
		const [articleStatus, setArticleStatus] = useState(status)
		const [articlePoster, setArticlePoster] = useState(image || defaultPoster)
		const [articleDescription, setArticleDescription] = useState(short_content)
		const [articleContent, setArticleContent] = useState(content)
		const [articleLanguage, setArticleLanguage] = useState(language)
		const [articleCategory, setArticleCategory] = useState(category_id)

		const router = useRouter()

		const handleSubmit = async (event) => {
			event.preventDefault()

			if (articleStatus !== 0) {
				const answer = confirm('Вы действительно хотите отправить статью на проверку?')

				if (!answer) {
					return false
				}
			}

			const articleId = router.query.id
			const passData = {
				title: articleTitle,
				publication_date: articleDate,
				status: articleStatus,
				image: articlePoster,
				short_content: articleDescription,
				content: articleContent,
				language: articleLanguage,
				category_id: articleCategory,
			}

			const cs = new CabinetService(session)
			const response = await cs.updateAuthorArticle(articleId, passData)

			if (response) {
				setNotification({
					text: 'Изменения сохранены.',
					status: 'success',
					visibility: true
				})

				setTimeout(() => {
					setNotification({
						visibility: false
					});
				}, 2000)

			} else {
				setNotification({
					text: 'Не удалось внести изменения, повторите позже.',
					status: 'error',
					visibility: true
				})

				setTimeout(() => {
					setNotification({
						visibility: false
					});
				}, 2000)
			}
		}

		return (
			<CabinetLayout title={'Редактирование статьи'}>
				{notification.visibility && notification.text && (
					<p className={`notification ${notification.status}`}>{notification.text}</p>
				)}

				<form onSubmit={handleSubmit} method="POST" className="my-4">
					<ArticleGeneralInfo
						title={articleTitle}
						date={new Date(articleDate * 1000)}
						poster={articlePoster}
						language={articleLanguage}
						category={articleCategory}
						languages={languages}
						categories={categories}
						description={articleDescription}
						handleTitle={(value) => {
							setArticleTitle(value)
						}}
						handleDate={(value) => {
							setArticleDate(Date.parse(value) / 1000)
						}}
						handlePoster={(value) => {
							setArticlePoster(value)
						}}
						handleLanguage={(value) => {
							setArticleLanguage(value)
						}}
						handleCategory={(value) => {
							setArticleCategory(value)
						}}
						handleDescription={(value) => {
							setArticleDescription(value)
						}}
					/>

					<ArticleTextEditor
						data={articleContent}
						handleContent={(value) => {
							setArticleContent(value)
						}}
					/>

					<FormButtons
						status={articleStatus}
						handleStatus={(value) => {
							setArticleStatus(value)
						}}
					/>
				</form>
			</CabinetLayout >
		)
	}

	return (
		<p onClick={signIn}>Войти в кабинет</p>
	)
}

export async function getServerSideProps(context) {
	const session = await getSession(context)
	const callBackUrl = `${context.req.headers.host}${context.req.url}`
	const articleId = context.query.id

	if (session) {
		const cs = new CabinetService(session)
		const data = await cs.getAuthorArticle(articleId)

		if (data) {
			return {
				props: {
					data,
				}
			}
		} else {
			return {
				redirect: {
					destination: '/cabinet',
					permanent: true,
				},
			}
		}
	}

	return {
		redirect: {
			// destination: `/api/auth/signin?callbackUrl=${callBackUrl}`,
			destination: `/cabinet/login?callbackUrl=${callBackUrl}`,
			permanent: false,
		},
	}
}
