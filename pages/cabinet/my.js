import CabinetLayout from '/components/CabinetLayout'
import UserInfo from '/components/layout/cabinet/UserInfo'
import Articles from '/components/layout/cabinet/Articles'
import { useSession, signIn, getSession } from 'next-auth/client'
import CabinetService from '/components/services/CabinetService'

export default function Cabinet({ data }) {
	const [session, loading] = useSession()
	const { articles, user, statuses } = data

	if (loading) {
		return null
	}

	if (session) {
		user.position = "Журналист"

		return (
			<CabinetLayout title={'Cabinet'}>
				<UserInfo data={user} />
				<Articles data={articles} statuses={statuses} />
			</CabinetLayout>
		)
	}

	return (
		<p onClick={signIn}>Войти в кабинет</p>
	)
}

export async function getServerSideProps(context) {
	const session = await getSession(context)
	const callBackUrl = `${context.req.headers.host}${context.req.url}`

	if (session) {
		const cs = new CabinetService(session)
		const data = await cs.getMyPage('all')

		return {
			props: {
				data,
			}
		}
	}

	return {
		redirect: {
			// destination: `/api/auth/signin?callbackUrl=${callBackUrl}`,
			destination: `/cabinet/login?callbackUrl=${callBackUrl}`,
			permanent: false,
		},
	}
}
