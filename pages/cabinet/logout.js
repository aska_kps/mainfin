import { signOut } from 'next-auth/client'

export default function Logout() {
    return (
        <p onClick={signOut}>Выйти из кабинет</p>
    )
}