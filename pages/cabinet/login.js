import style from '/styles/login.module.css'
import { getSession, getCsrfToken } from 'next-auth/client'
import { useState } from 'react'
import { useRouter } from 'next/router'

export default function Login({ csrfToken }) {
	const [inputType, setInputType] = useState("password")

	const handleInputType = () => {
		if (inputType == "password") {
			setInputType("text")
		} else if (inputType == "text") {
			setInputType("password")
		}
	}

	const router = useRouter()
	const authError = router.query.error || false

	return (
		<div className={style._wrapper}>
			<div className="container">
				<div className={style._body}>
					{/* <p className={style._title}>Вход в личный кабинет</p> */}

					<form method='post' action='/api/auth/callback/credentials' className={style._form}>
						<input name='csrfToken' type='hidden' defaultValue={csrfToken} />

						<div className={style._inputField}>
							<label>Имя пользователя</label>
							<input id="username" name="username" type="text" required placeholder="username" />
						</div>

						<div className={style._inputField}>
							<label>Пароль</label>
							<input id="password" name="password" type={inputType} required placeholder="password" />
							<div className={style._inputTypeToggler} onClick={handleInputType}>
								<img src="/icons/eye.svg" alt="" />
							</div>
						</div>

						{authError && (
							<p className={style._error}>Что-то пошло не так, попробуйте снова</p>
						)}

						<button className={style._button} type='submit'>Войти</button>
					</form>
				</div>
			</div>
		</div>
	)
}

export async function getServerSideProps(context) {
	const session = await getSession(context)

	if (session) {
		return {
			redirect: {
				destination: '/cabinet/my',
				permanent: true,
			},
		}
	}

	return {
		props: {
			csrfToken: await getCsrfToken(context)
		}
	}
}
