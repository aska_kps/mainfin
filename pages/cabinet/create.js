import CabinetLayout from '/components/CabinetLayout'
import CabinetService from '/components/services/CabinetService'
import ArticleTextEditor from '/components/layout/cabinet/ArticleTextEditor'
import ArticleGeneralInfo from '/components/layout/cabinet/ArticleGeneralInfo'
import FormButtons from '/components/layout/FormButtons'
import { useSession, signIn, getSession } from 'next-auth/client'
import { useState } from 'react'
import { useRouter } from 'next/router'

export default function Create({ data }) {
	const [session, loading] = useSession()

	if (loading) {
		return null
	}

	if (session) {
		const [notification, setNotification] = useState({
			text: '',
			status: '',
			visibility: false
		})

		const { languages, categories } = data
		const defaultPoster = '/images/poster.jpg'

		const [articleTitle, setArticleTitle] = useState('')
		const [articleDate, setArticleDate] = useState(Date.parse(new Date) / 1000)
		const [articleStatus, setArticleStatus] = useState(0)
		const [articlePoster, setArticlePoster] = useState(defaultPoster)
		const [articleDescription, setArticleDescription] = useState('')
		const [articleContent, setArticleContent] = useState('Начните печатать')
		const [articleLanguage, setArticleLanguage] = useState('ru')
		const [articleCategory, setArticleCategory] = useState(1)

		const router = useRouter()

		const handleSubmit = async (event) => {
			event.preventDefault()

			if (articleStatus !== 0) {
				const answer = confirm('Вы действительно хотите отправить статью на проверку?')

				if (!answer) {
					return false
				}
			}

			const articleId = router.query.id
			const passData = {
				title: articleTitle,
				publication_date: articleDate,
				status: articleStatus,
				image: articlePoster,
				short_content: articleDescription,
				content: articleContent,
				language: articleLanguage,
				category_id: articleCategory,
			}

			const cs = new CabinetService(session)
			const response = await cs.createAuthorArticle(passData)
			console.log(response)
			if (response) {
				setNotification({
					text: 'Изменения сохранены.',
					status: 'success',
					visibility: true
				})

				setTimeout(() => {
					setNotification({
						visibility: false
					});
				}, 2000)

			} else {
				setNotification({
					text: 'Не удалось внести изменения, повторите позже.',
					status: 'error',
					visibility: true
				})

				setTimeout(() => {
					setNotification({
						visibility: false
					});
				}, 2000)
			}
		}

		return (
			<CabinetLayout title={'Редактирование статьи'}>
				{notification.visibility && notification.text && (
					<p className={`notification ${notification.status}`}>{notification.text}</p>
				)}

				<form onSubmit={handleSubmit} method="POST" className="my-4">
					<ArticleGeneralInfo
						title={articleTitle}
						date={new Date(articleDate * 1000)}
						poster={articlePoster}
						language={articleLanguage}
						category={articleCategory}
						languages={languages}
						categories={categories}
						description={articleDescription}
						handleTitle={(value) => {
							setArticleTitle(value)
						}}
						handleDate={(value) => {
							setArticleDate(Date.parse(value) / 1000)
						}}
						handlePoster={(value) => {
							setArticlePoster(value)
						}}
						handleLanguage={(value) => {
							setArticleLanguage(value)
						}}
						handleCategory={(value) => {
							setArticleCategory(value)
						}}
						handleDescription={(value) => {
							setArticleDescription(value)
						}}
					/>

					<ArticleTextEditor
						data={'Начните печатать...'}
						handleContent={(value) => {
							setArticleContent(value)
						}}
					/>

					<FormButtons
						status={articleStatus}
						handleStatus={(value) => {
							setArticleStatus(value)
						}}
					/>
				</form>
			</CabinetLayout >
		)
	}

	return (
		<p onClick={signIn}>Войти в кабинет</p>
	)
}

export async function getServerSideProps(context) {
	const session = await getSession(context)
	const callBackUrl = `${context.req.headers.host}${context.req.url}`
	const articleId = context.query.id

	if (session) {
		const cs = new CabinetService(session)
		const data = await cs.getCommonData()

		return {
			props: {
				data,
			}
		}
	}

	return {
		redirect: {
			// destination: `/api/auth/signin?callbackUrl=${callBackUrl}`,
			destination: `/cabinet/login?callbackUrl=${callBackUrl}`,
			permanent: false,
		},
	}
}
